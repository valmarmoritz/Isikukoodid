﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Isikukoodid
{
    enum Sugu { Naine, Mees }
    static class Program
    {
        static Random rnd = new Random();
        static string Andmekataloog = @"..\..\andmed\";
        static void Main(string[] args)
        {
            //string isikukood = "35503070211";

            //do
            //{
            //    Console.Write("Anna isikukood (vähemalt 3 esimest numbrit): ");
            //    string isikukood = Console.ReadLine();
            //    isikukood = GenereeriIsikukood(isikukood.Substring(0, 3));
            //    Console.WriteLine("Genereeritud suvalise sünnikuupäevaga korrektne isikukood: " + isikukood);
            //    Console.WriteLine($"isikukoodiga {isikukood} inimene on {isikukood.ToSugu()} ja sündinud {isikukood.ToDatetime().ToShortDateString()}");
            //    Console.WriteLine(isikukood.Check() ? "õige" : "vale");
            //}
            //while (true);

            ParandaIsikukoodid(Andmekataloog);

        }

        static void ParandaIsikukoodid(string Andmekataloog)
        {
            Dictionary<string, string> Isikukoodid = new Dictionary<string, string>();
            string[] inFailid = { "opilased.txt", "opetajad.txt", "vanemad.txt" };
            string[] outFailid = { "opilased.txt", "opetajad.txt", "vanemad.txt", "oppealajuhataja.txt", "oppeained.txt", "klassid.txt", "hinded.txt"};
            
            foreach (var f in inFailid)
            {
                var otsinInimesi =
                    File.ReadAllLines(Andmekataloog + f, Encoding.Default)
                    .Select(x => x.Split(','))
                    .Select(x => x[0].Trim())
                    .ToList();
                foreach (var x in otsinInimesi) if (!Isikukoodid.ContainsKey(x)) Isikukoodid.Add(x, GenereeriIsikukood(x));
            }

            // Nüüd on olemas dictionary Isikukoodid, kus Key on (iga failidest leitud) isikukood ja Value on selle (vajadusel) parandatud vaste.
            Console.WriteLine("Andmed sisse loetud.");
            // foreach (var x in Isikukoodid.OrderBy(x => x.Key)) Console.WriteLine(x.Key + " " + x.Value);

            foreach (var f in outFailid)
            {
                var failiread = File.ReadAllText(Andmekataloog + f, Encoding.Default);
                foreach (string ik in Isikukoodid.Keys) failiread = failiread.Replace(ik, Isikukoodid[ik]);
                File.WriteAllText(Andmekataloog + "_" + f, failiread);
            }
            Console.WriteLine("Parandatud isikukoodid on failides, mille nime ette on lisatud '_'.");
        }

        static string GenereeriIsikukood(string isikukood)
        {
            if (isikukood.Check()) return isikukood;
            else
            {
                string algus = isikukood.Substring(0, 3);
                int päevAastas = (rnd.Next() % 365);
                string kuu = (päevAastas % 12 + 1).ToString("00");
                string päev = (päevAastas % 31 + 1).ToString("00");
                DateTime.TryParse(ToDatetime(algus + kuu + päev).ToShortDateString(), out DateTime sünnikuupäev);
                // Console.WriteLine("suvaline kuupäev sel aastal: "+sünnikuupäev.ToShortDateString());
                for (int kj = 0; kj < 10; kj++)
                {
                    isikukood = algus + kuu + päev + (rnd.Next() % 100).ToString("000") + kj.ToString();
                    if (isikukood.Check()) break;
                }
            }
            return isikukood;
        }
        static bool Check(this string isikukood)
        {
            int i = 0;

            int kj =
            isikukood.Substring(0, 10)
                .Select(x => x - '0')
                .ToArray()
                .Select(x => x * ((i++ % 9) + 1))
                .Sum()
                % 11
                ;
            if (kj == 10)
            {
                i = 2;
                kj = isikukood.Substring(0, 10).ToArray()
                    .Select(x => x * ((i++ % 9) + 1))
                .Sum() % 11 % 10;
            }

            return kj == isikukood[10] - '0';
        }

        static int Prindi(this int i)
        {
            Console.WriteLine();
            Console.WriteLine(i);
            return i;
        }
        static IEnumerable<int> Prindi(this IEnumerable<int> m)
        {
            Console.WriteLine();
            foreach (var x in m) { Console.Write($"{x} "); yield return x; }
        }

        static Sugu ToSugu(this String isikukood)
        {
            return (isikukood[0] - '0') % 2 == 0 ? Sugu.Naine : Sugu.Mees;
        }


        static DateTime ToDatetime(this string isikukood)
        {
            DateTime x;

            DateTime.TryParse(
                (((isikukood[0] - '0' - 1) / 2 + 18).ToString() +
                    isikukood.Substring(1, 2) + "-" +
                    isikukood.Substring(3, 2) + "-" +
                    isikukood.Substring(5, 2))
                    , out x)
                    ;
            return x;
        }
    }
}
